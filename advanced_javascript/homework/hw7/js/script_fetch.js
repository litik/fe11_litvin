class FetchRequest {
    request = async function (url) {
        let response = await fetch(url);
        if(!response.ok) {
            console.log("Looks like there was a problem. Status Code: " +  response.status);  
            return;
        } else {
            let reply = await response.json();
            return reply; 
        }
    }
}

function getFilmsByFetch() {
    let fetchRequest = new FetchRequest();
    fetchRequest.request("https://swapi.co/api/films/")
        .then( (result) => {
            result.results.forEach( (elem, index, array) => {
                const div = document.createElement("div");
                div.innerHTML = `<h2>${elem.title}</h2>
                                <h6>Episode: ${elem.episode_id}</h6>
                                <span>${elem.opening_crawl}</span><br><br>
                                <span>List of Characters:</span><br>`
                document.body.append(div);
               /*  console.log(elem.characters); */
                Promise.all( elem.characters.map(url => {
                    fetchRequest.request(url);
                    /* console.log(fetchRequest.request(url)); */
                    return fetchRequest.request(url);
                }) ) 
                .then((characters) => {
                    characters.forEach(elem => {
                        const list = document.createElement("li");
                        list.innerHTML = elem.name;
                        div.append(list);
                        /* console.log(elem.name); */
                    })
                })
        })
    })
}

/* getfilmsByFetch(); */




