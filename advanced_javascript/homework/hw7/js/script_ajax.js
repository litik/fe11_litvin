function getFilmsbyAjax(url) {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.responseType = "json";
    xhr.send();
    xhr.onload = function() {
        if(xhr.status === 200) {
            xhr.response.results.forEach(elem => {
                const div = document.createElement("div");
                div.innerHTML = `<h2>${elem.title}</h2>
                                <h6>Episode: ${elem.episode_id}</h6>
                                <span>${elem.opening_crawl}</span><br><br>
                                <span>List of Characters:</span><br>`
                document.body.append(div);
                
                elem.characters.forEach(url => {
                    let xhr2 = new XMLHttpRequest();
                    xhr2.open("GET", url, true);
                    xhr2.responseType = "json";
                    xhr2.send();
                    xhr2.onload = function() {
                        if(xhr2.status === 200) {
                            const list = document.createElement("li");
                            list.innerHTML = xhr2.response.name;
                            div.append(list);
                         } else {
                           console.log("Ошибка " + xhr.status + ": " + xhr.statusText)
                      }
                    }  
                })
            });
        }
    }
}

/* getFilmsbyAjax("https://swapi.co/api/films/"); */

