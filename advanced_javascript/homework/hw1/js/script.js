Hamburger.SIZE_SMALL = { size: 'small', price: 50, calories: 20 };
Hamburger.SIZE_LARGE = { size: 'large', price: 100,calories: 40 };
Hamburger.STUFFING_CHEESE = { stuffing:'cheese', price: 10, calories: 20 };
Hamburger.STUFFING_SALAD = { stuffing: 'salad', price: 20,  calories: 5 };
Hamburger.STUFFING_POTATO = { stuffing: 'potato', price: 15, calories: 10 };
Hamburger.TOPPING_MAYO = { topping: 'mayonnaise', price: 20, calories : 5 };
Hamburger.TOPPING_SPICE = { topping: 'spice', price : 15, calories : 0 };

function Hamburger(size, stuffing) {
    try {
        if (!size) {
            throw  new Error('Please specify your Hamburger size');
        }
        if (!stuffing){
            throw new Error('Please specify your Hamburger stuffing');
        }
        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];
    } catch (err) {
        console.log(err.message);
    }
}

Hamburger.prototype.addTopping = function (topping) {
    try {
       this.toppings.forEach(function (item) {
            if (item === topping) {
                throw new Error("Warning! You have already added this type of topping")
            }
        });
        this.toppings.push(topping);
    } catch (err) {
        console.log(err.message);
    }
};

Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (this.toppings.length === 0) {
            throw new Error("Warning! No toppings were added")
            }
        this.toppings.forEach(function (item, i, toppings) {
            if (item === topping) {
                toppings.splice(i, 1);
            }
        })
    } catch (err) {
        console.log(err.message)
    }
};

Hamburger.prototype.getToppings = function () {
    return this.toppings;
};

Hamburger.prototype.getSize = function () {
    return this.size;
};

Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
};

Hamburger.prototype.calculatePrice = function () {
    var allInPrice = 0;
    allInPrice += this.size.price;
    allInPrice += this.stuffing.price;
    allInPrice += this.toppings.reduce(function (total, item) {
        return total + item.price;
    }, 0);
    console.log("Your newBurger price is - " + allInPrice);
    return allInPrice;
};

Hamburger.prototype.calculateCalories = function () {
    var allInCalories = 0;
    allInCalories += this.size.calories;
    allInCalories += this.stuffing.calories;
    allInCalories += this.toppings.reduce(function (total, item) {
        return total + item.calories;
    }, 0);
    console.log("Your newBurger calories are - " + allInCalories );
    return allInCalories;
};

function Error (message) {
    this.message = message;
}

var newBurger = new Hamburger (Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

newBurger.addTopping(Hamburger.TOPPING_MAYO);
// newBurger.addTopping(Hamburger.TOPPING_MAYO);
newBurger.addTopping(Hamburger.TOPPING_SPICE);

newBurger.removeTopping(Hamburger.TOPPING_MAYO);
// newBurger.removeTopping(Hamburger.TOPPING_SPICE);

newBurger.getToppings();
newBurger.getSize();
newBurger.getStuffing();

newBurger.calculatePrice();
newBurger.calculateCalories();

console.log(newBurger);
