class Hamburger {
    constructor(size, stuffing) {
        try {
            if (!size) {
                throw new Error('Please specify your Hamburger size');
            } else if (!stuffing) {
                throw new Error('Please specify your Hamburger stuffing');
            } else {
                this.size = size;
                this.stuffing = stuffing;
                this.toppings = [];
            }
        } catch (err) {
                console.log(err.message);
            }
        }

    addTopping(topping) {
        try {
            this.toppings.forEach(function (item) {
                if (item === topping) {
                    throw new Error("Warning! You have already added this type of topping")
                }
            });
            this.toppings.push(topping);
        } catch (err) {
            console.log(err.message);
        }
    }

    removeTopping(topping) {
        try {
            if (this.toppings.length === 0) {
                throw new Error("Warning! No toppings were added")
            }
            this.toppings.forEach(function (item, i, toppings) {
                if (item === topping) {
                    toppings.splice(i, 1);
                }
            })
        } catch (err) {
            console.log(err.message)
        }
    }

    getToppings (toppings) {
        return this.toppings;
    };

    getSize (size) {
        return this.size;
    };

    getStuffing (stuffing) {
        return this.stuffing;
    };

    calculatePrice() {
        let allInPrice = 0;
        allInPrice += this.size.price;
        allInPrice += this.stuffing.price;
        allInPrice += this.toppings.reduce(function (total, item) {
            return total + item.price;
        }, 0);
        console.log("Your newBurger price is - " + allInPrice);
        return allInPrice;
    };

    calculateCalories () {
        let allInCalories = 0;
        allInCalories += this.size.calories;
        allInCalories += this.stuffing.calories;
        allInCalories += this.toppings.reduce(function (total, item) {
            return total + item.calories;
        }, 0);
        console.log("Your newBurger calories are - " + allInCalories );
        return allInCalories;
    };

    static SIZE_SMALL = { size: 'small', price: 50, calories: 20 };
    static SIZE_LARGE = { size : 'large', price: 100, calories: 40 };
    static STUFFING_CHEESE = { stuffing : 'cheese', price: 10, calories: 20 };
    static STUFFING_SALAD = { stuffing : 'salad', price: 10, calories: 5 };
    static STUFFING_POTATO = { stuffing : 'potato', price: 15, calories: 10 };
    static TOPPING_MAYO = { topping : 'mayonnaise',price: 20, calories: 5 };
    static TOPPING_SPICE = { topping: 'spice', price: 15, calories: 0 };
}

function Error (message) {
    this.message = message;
}

// маленький гамбургер с начинкой из сыра
let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1

console.log(hamburger);
